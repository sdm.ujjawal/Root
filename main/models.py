from django.db import models

# Create your models here.
class Country(models.Model):
    country_name = models.CharField(max_length=200)
    country_capital = models.CharField(max_length=200)
    country_currency = models.CharField(max_length=200)
    country_language = models.CharField(max_length=200)

    def __str__(self):
        return self.country_name
